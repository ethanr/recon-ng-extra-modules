from recon.core.module import BaseModule
import codecs
import datetime
import os
from collections import OrderedDict

class Module(BaseModule):

    meta = {
        'name': 'HTML Credentials Report Generator',
        'author': 'Ethan Robish (@EthanRobish) and Tim Tomes (@LaNMaSteR53)',
        'description': 'Creates a HTML report with leaked credentials in it.',
        'options': (
            ('sanitize', True, True, 'mask sensitive data in the report'),
            ('customer', None, True, 'customer name for the report header'),
            ('creator', None, True, 'creator name for the report footer'),
            ('filename', os.path.join(BaseModule.workspace, 'results.html'), True, 'path and filename for report output'),
        ),
    }

    table_show = '<a id="show-{table}" href="javascript:showhide(\'{table}\');"><p>[+] {desc}</p></a>'
    table_hide = '<a id="hide-{table}" href="javascript:showhide(\'{table}\');"><p>[-] {desc}</p><hr></a>'

    def flatten(self, data, index=0, remove=['module']):
        return [d[index] for d in data if d[index] not in remove]

    def safe_str(self, data):
        return self.html_escape(self.to_unicode_str(data)) if data is not None else u''

    def build_summary_table(self):
        return u'''
        <div class="container">
          {table_show}
          {table_hide}
          <table id="summary">
            <tr>
              <th>table</th>
              <th>count</th>
            </tr>
            <tr>
              <td>credentials</td>
              <td class="centered">{creds_count}</td>
            </tr>
            <tr>
              <td>leaks</td>
              <td class="centered">{leaks_count}</td>
            </tr>
          </table>
        </div>
        <br />
        '''.format(
            table_show=self.table_show.format(table='summary', desc='Summary'),
            table_hide=self.table_hide.format(table='summary', desc='Summary'),
            creds_count=len(self.credentials),
            leaks_count=len(self.leaks)
        )

    def build_credentials_table(self):
        if not self.credentials: return ''

        _ = lambda x: self.safe_str(x)  # create a shortcut alias for safe_str

        headers = self.credentials[0].keys()
        headers.remove('type')
        row_headers = '''
        <tr>
          <th>%s</th>
        </tr>
        ''' % '</th><th>'.join(headers)

        rows = []
        for cred in self.credentials:
            leak = self.leaks[cred['leak']]
            if cred['password'] and self.options['sanitize']:
                cred['password'] = u'<omitted>'
            rows.append(u'''
                <tr>
                  <td>{username}</td>
                  <td>{password}</td>
                  <td title="Hash Type: {hashtype}">{hash}</td>
                  <td><a href="#{leak}" title="{description}">{title}</a></td>
                </tr>
                '''.format(
                    username=_(cred['username']),
                    password=_(cred['password']),
                    hashtype=_(cred['type']),
                    hash=_(cred['hash']),
                    leak=_(cred['leak']),
                    description=_(leak['description']),
                    title=_(leak['title'])
                )
            )

        table_content = u'''
        <div class="container">
          {table_show}
          {table_hide}
          <table name="table" id="credentials">
            {headers}
            {body}
          </table>
        </div>
        <br />
        '''.format(
            table_show=self.table_show.format(table='credentials', desc='Credentials'),
            table_hide=self.table_hide.format(table='credentials', desc='Credentials'),
            headers=row_headers,
            body=''.join(rows)
        )
        return table_content

    def build_leaks_table(self):
        if not self.leaks: return ''

        _ = lambda x: self.safe_str(x)  # create a shortcut alias for safe_str

        rows = []
        for leak in self.leaks.values():
            leak_details = []
            for key, value in leak.items():
                leak_details.append(u'''
                    <tr>
                      <td><strong>%s</strong></td>
                      <td>%s</td>
                    </tr>\n
                    ''' % (_(key), _(value))
                )

            leak_id = leak['leak_id']
            affected_users = [_(cred['username']) for cred in self.credentials if cred['leak'] == leak_id]

            rows.append(u'''
                <hr>
                <table id="{leak_id}" class="leak">
                  {leak_details}
                  <tr>
                    <td><strong>affected</strong></td>
                    <td>
                      <a id="show-{leak_id}-affected" href="javascript:showhide(\'{leak_id}-affected\');"><p>[+] Affected Users</p></a>
                      <a id="hide-{leak_id}-affected" href="javascript:showhide(\'{leak_id}-affected\');"><p>[-] Affected Users</p></a>
                      <ul id="{leak_id}-affected" name="table">
                        <li>{affected_users}</li>
                      </ul>
                    </td>
                  </tr>
                </table>
                '''.format(
                    leak_details=''.join(leak_details),
                    leak_id=_(leak_id),
                    affected_users='</li><li>'.join(affected_users)
                )
            )

        return u'''
        <div class="container">
          {table_show}
          {table_hide}
          <!-- we want the leaks table to be expanded by default -->
          <div id="leaks" >
            {leaks}
          </div>
        </div>
        <br />
        '''.format(
            table_show=self.table_show.format(table='leaks', desc='Associated Leaks'),
            table_hide=self.table_hide.format(table='leaks', desc='Associated Leaks'),
            leaks=''.join(rows)
        )

    def module_run(self):
        cred_headers = self.flatten(self.query('PRAGMA table_info(credentials)'), 1)
        leak_headers = self.flatten(self.query('PRAGMA table_info(leaks)'), 1)

        sql_creds = self.query('SELECT "%s" FROM credentials ORDER BY username' % ('", "'.join(cred_headers)))
        sql_leaks = self.query('SELECT "%s" FROM leaks WHERE leak_id IN (SELECT leak FROM credentials) ORDER BY date(leak_date) DESC' % ('", "'.join(leak_headers)))

        self.credentials = [OrderedDict(zip(cred_headers, cred)) for cred in sql_creds]
        self.leaks = OrderedDict()
        for sql_leak in sql_leaks:
            leak = OrderedDict(zip(leak_headers, sql_leak))
            self.leaks[leak['leak_id']] = leak

        # Prevent accidentally generating a report without associated leaks. But allow it if there are no credentials.
        if self.credentials and not self.leaks:
            self.error('No leaks found in the database. Please run the \'leaks_dump\' module to populate the database and try again.')
            return

        filename = self.options['filename']
        with codecs.open(filename, 'wb', encoding='utf-8') as outfile:
            template = open(os.path.join(self.data_path, 'template_html.html')).read()

            self.verbose('Building Summary Table...')
            summary_table = self.build_summary_table()
            self.verbose('Building Credentials Table...')
            credentials_table = self.build_credentials_table()
            self.verbose('Building Leaks Table...')
            leaks_table = self.build_leaks_table()

            self.verbose('Compiling Report...')
            title = self.options['customer']
            creator = self.options['creator']
            created = datetime.datetime.now().strftime('%a, %b %d %Y %H:%M:%S')
            markup = template % (title, summary_table + credentials_table + leaks_table, creator, created)
            outfile.write(markup)
        self.output('Report generated at \'%s\'.' % (filename))
